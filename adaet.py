# -*- coding: utf-8 -*-
from __future__ import print_function
from colors import Colors
import sys, os
import subprocess
import time
import sqlite3
import csv
import datetime
import Orange
import matlab.engine



def load_intro():
    print ("\n\n")
    print (Colors.TITLE + "ADAET, implemented by Konstantia Barmpatsalou" + Colors.ENDC)
    print (Colors.TITLE + "Supervisors: Edmundo Monteiro, Paulo Simoes" + Colors.ENDC)
    print(Colors.TITLE + "Assistant Supervisor: Tiago Cruz" + Colors.ENDC)
    print ("\n")


class Menu():
    def __init__(self):
        self.choices = {
            "0": self.quit,
            "1": self.device_test,
            "2": self.extract_calls,
            "3": self.extract_sms,
            "4": self.create_calls_dataset,
            "5": self.create_sms_dataset,
            "6": self.calls_prep,
            "7": self.sms_prep,
            "8": self.cb_calls_gtgen,
            "9": self.cb_sms_gtgen,
            "10": self.cb_calls_neuralnet,
            "11": self.cb_sms_neuralnet,
            "12": self.cb_calls_acc,
            "13": self.cb_sms_acc,
            "14": self.dd_calls_gtgen,
            "15": self.dd_sms_gtgen,
            "16": self.dd_calls_neuralnet,
            "17": self.dd_sms_neuralnet,
            "18": self.dd_calls_acc,
            "19": self.dd_sms_acc
        }

    def display_menu(self):
        print ("************************************************************")
        print ("                        Menu                                ")
        print ("************************************************************")
        print ("0.  Quit")
        print ("1.  Test Device")
        print ("2.  Extract Calls")
        print ("3.  Extract SMS")
        print ("4.  Create Calls Dataset")
        print ("5.  Create SMS Dataset")
        print ("6.  Calls Preprocessing")
        print ("7.  SMS Preprocessing")
        print ("8.  Cyberbullying: Calls GT Generation")
        print ("9.  Cyberbullying: SMS GT Generation")
        print ("10. Cyberbullying: Calls Neural Network")
        print ("11. Cyberbullying: SMS Neural Network ")
        print ("12. Cyberbullying: Calls Calculate Accuracy")
        print ("13. Cyberbullying: SMS Calculate Accuracy")
        print ("14. Drug dealing: Calls GT Generation")
        print ("15. Drug dealing: SMS GT Generation")
        print ("16. Drug dealing: Calls Neural Network")
        print ("17. Drug dealing: SMS Neural Network")
        print ("18. Drug Dealing: Calls Calculate Accuracy")
        print ("19. Drug Dealing: SMS Calculate Accuracy")
        print ("************************************************************")

    def run(self):
        while True:
            self.display_menu()
            choice = input("Enter an option: ")
            action = self.choices.get(choice)
            if action:
                action()
            else:
                print (Colors.WARNING + "Not a valid option." + Colors.ENDC)

    def quit(self):
        print ("\n")
        print (Colors.WARNING + "Gracefully Quitting..." + Colors.ENDC)
        sys.exit(0)

    def device_test(self):
        print ("---------------------------------------")

        #Check if the device is connected


        print ("---------------------------------------")

    def extract_sms(self):
        #paths to databases need to be changed
        print ("---------------------------------------")
        print (Colors.WARNING + "Fetching SMS" + Colors.ENDC)
        print ("------------")
        procId = subprocess.Popen('c:/platform-tools/adb shell', stdin=subprocess.PIPE, universal_newlines=True)
        procId.communicate("su -c 'cp /data/data/com.android.providers.telephony/databases/mmssms.db  /storage/sdcard0/mmssms.db'; exit\n")
        time.sleep(1)
        subprocess.call("C:/platform-tools/adb pull /storage/sdcard0/mmssms.db C:\\Users\\tina\\Desktop")
        print ("---------------------------------------")

    def extract_calls(self):
        print("---------------------------------------")
        print(Colors.WARNING + "Fetching Calls" + Colors.ENDC)
        print("------------")
        procId = subprocess.Popen('c:/platform-tools/adb shell', stdin=subprocess.PIPE, universal_newlines=True)
        procId.communicate("su -c 'cp /data/data/com.android.providers.contacts/databases/contacts2.db  /storage/sdcard0/contacts2.db'; exit\n")
        subprocess.call("C:/platform-tools/adb pull /storage/sdcard0/contacts2.db C:\\Users\\tina\\Desktop")
        print("---------------------------------------")

    def create_sms_dataset(self):
        dbfile = "C:\\Users\\tina\\Desktop\\mmssms.db"
        conn = sqlite3.connect(dbfile)
        conn.text_factory = str
        cur = conn.cursor()
        data = cur.execute("SELECT thread_id, address, date, type, length(body) FROM sms")

        with open('C:\\Users\\tina\\Desktop\\rawSMS.csv',  "w", encoding='utf-8', newline="\n") as f:
            writer = csv.writer(f)
            writer.writerow(['thread_id', 'address', 'date', 'type', 'body'])
            writer.writerows(data)

    def create_calls_dataset(self):
        dbfile = "C:\\Users\\tina\\Desktop\\contacts2.db"
        conn = sqlite3.connect(dbfile)
        conn.text_factory = str
        cur = conn.cursor()
        data = cur.execute("SELECT number, duration, date, type, geocoded_location FROM calls")

        with open('C:\\Users\\tina\\Desktop\\Current Research\\rawCalls.csv', "w", encoding='utf-8', newline="\n") as f:
            writer = csv.writer(f)
            writer.writerow(['number', 'duration', 'date', 'type', 'geocoded_location'])
            writer.writerows(data)

    def sms_prep(self):
        infile='C:\\Users\\tina\\Desktop\\rawSMS.csv'
        finalSMSinp= 'C:\\Users\\tina\\Desktop\\finalSMSinp.csv'
        FMT = '%H:%M:%S'
        pre, afreq, sr, dt, tm, timecat, dfreq, cc, mob, smslen=([] for i in range(10))

        with open(infile, 'rt') as f:
            reader = csv.reader(f, delimiter=',')
            next(f)

            for row in reader:
                #create appearance frequency list
                pre.append(row[0])


                dt.append(datetime.datetime.fromtimestamp(float(str(row[2])[0:-3])).strftime('%Y-%m-%d'))
                tm.append(datetime.datetime.fromtimestamp(float(str(row[2])[0:-3])).strftime('%H:%M:%S'))


                #create cc
                if row[1].startswith("+351") or row[1].startswith("96") or row[1].startswith("93") or row[1].startswith("91"):
                    cc.append(2)
                elif row[1].isupper() or row[1].islower():
                    cc.append(1)
                else:
                    cc.append(0)

                #create mobility
                if row[1].startswith("96") or row[1].startswith("93") or row[1].startswith("91") or row[1].startswith("+3069") or row[1].startswith("+3519"):
                    mob.append(2)
                elif row[1].isupper() or row[1].islower() or len(row[1]) in range(1,7):
                    mob.append(1)
                else:
                    mob.append(0)


                #create send/received
                if row[3]=='1':
                    sr.append(1)
                elif row[3]=='2':
                    sr.append(0)

                #create length list
                smslen.append(int(row[4]))

            #print(sr)


            #calculate appearance frequency
            for i in pre:
                afreq.append(pre.count(i))
            #print(afreq)

            #calculate date frequency
            for k in dt:
                dfreq.append(dt.count(k))
            #print(dfreq)

            #calculate date category
            for l in tm:

                if time.strptime(l, FMT) < time.strptime("12:00:00", FMT) and time.strptime(l, FMT) > time.strptime("05:00:00", FMT):
                    timecat.append(0)
                elif time.strptime(l, FMT) < time.strptime("17:00:00", FMT) and time.strptime(l, FMT) > time.strptime("12:00:00", FMT):
                    timecat.append(1)
                elif time.strptime(l, FMT) < time.strptime("22:00:00", FMT) and time.strptime(l, FMT) > time.strptime("17:00:00", FMT):
                    timecat.append(2)
                elif time.strptime(l, FMT) < time.strptime("05:00:00", FMT) or time.strptime(l, FMT) > time.strptime("22:00:01", FMT):
                    timecat.append(3)

                else:
                    print(l)


            #print(timecat)

            alltogether = zip(dfreq, timecat, sr, afreq,smslen, cc, mob)

            with open(finalSMSinp, "w", newline="\n") as f:
                writer = csv.writer(f)
                #writer.writerow(['Date', 'Time', 'S/R', 'Appearance Frequency', 'Length', 'CC', 'Mobility'])
                for row in alltogether:
                    writer.writerow(row)

    def calls_prep(self):
        infile = 'C:\\Users\\tina\\Desktop\\Current Research\\rawCalls.csv'
        finalCallsinp = 'C:\\Users\\tina\\Desktop\\Current Research\\finalCallsinp.csv'
        FMT = '%H:%M:%S'
        pre, afreq, sr, dt, tm, timecat, dfreq, cc, mob, duration, pre2 = ([] for i in range(11))

        with open(infile, 'rt') as f:
            reader = csv.reader(f, delimiter=',')
            next(f)

            for row in reader:

                # create appearance frequency list
                pre.append(row[0])

                dt.append(datetime.datetime.fromtimestamp(float(str(row[2])[0:-3])).strftime('%Y-%m-%d'))
                tm.append(datetime.datetime.fromtimestamp(float(str(row[2])[0:-3])).strftime('%H:%M:%S'))

                # create mobility
                if row[0].startswith("96") or row[0].startswith("93") or row[0].startswith("91") or row[
                    0].startswith("+3069") or row[0].startswith("+3519") or row[0].startswith("003069") or row[0].startswith("003519") or row[0].startswith("92"):
                    mob.append(2)
                    #print (row[0])
                elif len(row[0]) in range(1, 7):
                    mob.append(1)
                    #print(row[0])
                else:
                    mob.append(0)
                    #print (row[0])

                #cc definition
                if "Portugal" in row[4] or "Coimbra" in row[4] or "Lisbon" in row[4] or "Lisboa" in row[4] or "Porto" in row[4]:
                    cc.append(2)
                elif row[4]:
                    cc.append(0)
                else:
                    cc.append(1)

                #incoming or outgoing call
                if "1" in row[3] or "3" in row[3]:
                    sr.append(1)
                else:
                    sr.append(0)

                #duration list
                if int(row[1])== 0:
                    duration.append(-1)
                else:
                    duration.append(row[1])




                # calculate time category
            for l in tm:

                if time.strptime(l, FMT) < time.strptime("12:00:00", FMT) and time.strptime(l, FMT) > time.strptime("05:00:00", FMT):
                    timecat.append(0)
                elif time.strptime(l, FMT) < time.strptime("17:00:00", FMT) and time.strptime(l,FMT) > time.strptime("12:00:00", FMT):
                    timecat.append(1)
                elif time.strptime(l, FMT) < time.strptime("22:00:00", FMT) and time.strptime(l,FMT) > time.strptime("17:00:00", FMT):
                    timecat.append(2)
                elif time.strptime(l, FMT) < time.strptime("05:00:00", FMT) or time.strptime(l,FMT) > time.strptime("22:00:01", FMT):
                    timecat.append(3)

                else:
                    print(l)
            #print(mob)

            #number normalization for af
            for i in pre:
                if i.startswith("+30"):
                   pre2.append(i[3:])
                elif i.startswith("+351") or i.startswith("0030"):
                   pre2.append(i[4:])
                elif  i.startswith("00351"):
                   pre2.append(i[5:])
                else:
                    pre2.append(i)

            #print (pre2)

            #appearance frequency
            for x in pre2:
                afreq.append(pre.count(x))
            #print(afreq)

            #daily frequency
            for k in dt:
                dfreq.append(dt.count(k))
            #print(dfreq)


            alltogetherc=zip(dfreq, timecat, sr, afreq, mob, cc, duration)
            with open(finalCallsinp, "w", newline="\n") as f:
                writer = csv.writer(f)
                #writer.writerow(['Date', 'Time', 'S/R', 'Appearance Frequency', 'Length', 'CC', 'Mobility'])
                for row in alltogetherc:
                    writer.writerow(row)

    def cb_calls_gtgen(self):
        fin = "C:\\Users\\tina\\Desktop\\Current Research\\finalCallsinp.csv"
        fout = "C:\\Users\\tina\\Desktop\\Current Research\\Datasets\\Cyberbullying\\Calls\\samsout_dd_calls_gt.csv"
        p = []

        with open(fin, 'r+') as f:
            lines = f.readlines()
            for i in range(0, len(lines)):

                line = lines[i].split(",")

                # Checks for outgoing call
                if line[2] == "0":
                    # Checks for foreign CC and fixed line Mobility. Also, it will check for local cc and fixed mobility, unknown cc and fixed mobility
                    if ("0" in line[4] and "0" in line[5]) or ("0" in line[4] and "1" in line[5]) or (
                                    "2" in line[4] and "0" in line[5]) or ("1" in line[4] and "0" in line[5]) or (
                                    "0" in line[4] and "2" in line[5]):
                        p.append("0.15")
                        # print(line)
                    elif ("2" in line[4] and "2" in line[5]):
                        # Checks for suspicious sent messages
                        if ("3" in line[1]):
                            if int(line[0]) >= 10 and int(line[3]) in range(29, 46) and (
                                    "-1" in line[6] or int(line[6]) in range(0, 15)):
                                p.append("1")
                                # print(line)
                            elif (int(line[0]) in range(6, 10) and int(line[3]) in range(29, 46) and (
                                    "-1" in line[6] or int(line[6]) in range(0, 15))) or (
                                        int(line[0]) >= 10 and (int(line[3]) >= 46 or int(line[3]) < 29) and (
                                    "-1" in line[6] or int(line[6]) in range(0, 15))) or (
                                        int(line[0]) >= 10 and int(line[3]) in range(29, 46) and int(line[6]) in range(
                                    15, 30)):
                                p.append("0.75")
                                # print(line)
                            elif (int(line[0]) in range(6, 10) and (int(line[3]) >= 46 or int(line[3]) < 29) and (
                                    "-1" in line[6] or int(line[6]) in range(0, 15))) or (
                                        int(line[0]) in range(6, 10) and int(line[3]) in range(29, 46) and int(
                                    line[6]) in range(15, 30)) or (
                                        int(line[0]) >= 10 and (int(line[3]) >= 46 or int(line[3]) < 29) and int(
                                    line[6]) in range(15, 30)) or (int(line[0]) in range(6, 10) and (
                                    int(line[3]) >= 46 or int(line[3]) < 29) and int(line[6]) in range(15, 30)):
                                p.append("0.5")
                                # print(line)
                            else:
                                p.append("0.25")
                                # print(line)
                        elif ("2" in line[1] or "1" in line[1]):
                            if int(line[0]) >= 10 and int(line[3]) in range(29, 46) and (
                                            "-1" in line[6] or int(line[6]) in range(0, 15)):
                                p.append("0.75")
                                # print(line)
                            elif (int(line[0]) in range(6, 10) and int(line[3]) in range(29, 46) and (
                                    "-1" in line[6] or int(line[6]) in range(0, 15))) or (
                                        int(line[0]) >= 10 and (int(line[3]) >= 46 or int(line[3]) < 29) and (
                                    "-1" in line[6] or int(line[6]) in range(0, 15))) or (
                                        int(line[0]) >= 10 and int(line[3]) in range(29, 46) and int(line[6]) in range(
                                    15, 30)):
                                p.append("0.5")
                                # print(line)
                            elif (int(line[0]) in range(6, 10) and (int(line[3]) >= 46 or int(line[3]) < 29) and (
                                    "-1" in line[6] or int(line[6]) in range(0, 15))) or (
                                        int(line[0]) in range(6, 10) and int(line[3]) in range(29, 46) and int(
                                    line[6]) in range(15, 30)) or (
                                        int(line[0]) >= 10 and (int(line[3]) >= 46 or int(line[3]) < 29) and int(
                                    line[6]) in range(15, 30)) or (int(line[0]) in range(6, 10) and (
                                    int(line[3]) >= 46 or int(line[3]) < 29) and int(line[6]) in range(15, 30)):
                                p.append("0.25")
                                # print(line)
                            else:
                                p.append("0.15")
                                # print(line)
                        elif "0" in line[1]:
                            if int(line[0]) >= 10 and int(line[3]) in range(29, 46) and (
                                            "-1" in line[6] or int(line[6]) in range(0, 15)):
                                p.append("0.5")
                                # print(line)
                            elif (int(line[0]) in range(6, 10) and int(line[3]) in range(29, 46) and (
                                    "-1" in line[6] or int(line[6]) in range(0, 15))) or (
                                        int(line[0]) >= 10 and (int(line[3]) >= 46 or int(line[3]) < 29) and (
                                    "-1" in line[6] or int(line[6]) in range(0, 15))) or (
                                        int(line[0]) >= 10 and int(line[3]) in range(29, 46) and int(line[6]) in range(
                                    15, 30)):
                                p.append("0.25")
                                # print(line)
                            else:
                                p.append("0.15")
                                # print(line)
                    elif ("1" in line[4] and "1" in line[5]) or ("1" in line[4] and "2" in line[5]) or (
                            "2" in line[4] and "1" in line[5]):
                        # print(line)
                        if "3" in line[1]:
                            if int(line[0]) >= 10 and int(line[3]) in range(29, 46) and (
                                            "-1" in line[6] or int(line[6]) in range(0, 15)):
                                p.append("0.75")
                                # print(line)
                            elif (int(line[0]) in range(6, 10) and int(line[3]) in range(29, 46) and (
                                    "-1" in line[6] or int(line[6]) in range(0, 15))) or (
                                        int(line[0]) >= 10 and (int(line[3]) >= 46 or int(line[3]) < 29) and (
                                    "-1" in line[6] or int(line[6]) in range(0, 15))) or (
                                        int(line[0]) >= 10 and int(line[3]) in range(29, 46) and int(line[6]) in range(
                                    15, 30)):
                                p.append("0.5")
                                # print(line)
                            elif (int(line[0]) in range(6, 10) and (int(line[3]) >= 46 or int(line[3]) < 29) and (
                                    "-1" in line[6] or int(line[6]) in range(0, 15))) or (
                                        int(line[0]) in range(6, 10) and int(line[3]) in range(29, 46) and int(
                                    line[6]) in range(15, 30)) or (
                                        int(line[0]) >= 10 and (int(line[3]) >= 46 or int(line[3]) < 29) and int(
                                    line[6]) in range(15, 30)) or (int(line[0]) in range(6, 10) and (
                                    int(line[3]) >= 46 or int(line[3]) < 29) and int(line[6]) in range(15, 30)):
                                p.append("0.25")
                                # print(line)
                            else:
                                p.append("0.15")
                                # print(line)
                        elif ("2" in line[1] or "1" in line[1]):
                            if int(line[0]) >= 10 and int(line[3]) in range(29, 46) and (
                                            "-1" in line[6] or int(line[6]) in range(0, 15)):
                                p.append("0.5")
                                # print(line)
                            elif (int(line[0]) in range(6, 10) and int(line[3]) in range(29, 46) and (
                                    "-1" in line[6] or int(line[6]) in range(0, 15))) or (
                                        int(line[0]) >= 10 and (int(line[3]) >= 46 or int(line[3]) < 29) and (
                                    "-1" in line[6] or int(line[6]) in range(0, 15))) or (
                                        int(line[0]) >= 10 and int(line[3]) in range(29, 46) and int(line[6]) in range(
                                    15, 30)):
                                p.append("0.25")
                                # print(line)
                            else:
                                p.append("0.15")
                        elif "0" in line[1]:
                            if int(line[0]) >= 10 and int(line[3]) in range(29, 46) and (
                                            "-1" in line[6] or int(line[6]) in range(0, 15)):
                                p.append("0.25")
                            else:
                                p.append("0.15")
                    else:
                        print(line)
                        a = 0;
                elif line[2] == "1":
                    # Checks for foreign CC and fixed line Mobility. Also, it will check for local cc and fixed mobility, unknown cc and fixed mobility
                    if ("0" in line[4] and "0" in line[5]) or ("0" in line[4] and "1" in line[5]) or (
                                    "2" in line[4] and "0" in line[5]) or ("1" in line[4] and "0" in line[5]) or (
                                    "0" in line[4] and "2" in line[5]):
                        p.append("0.15")
                        # print(line)
                    elif ("2" in line[4] and "2" in line[5]):
                        # Checks for suspicious sent messages
                        # print(line)
                        if ("3" in line[1]):
                            if int(line[0]) >= 10 and int(line[3]) in range(29, 46) and "-1" in line[6]:
                                p.append("1")
                                # print(line)
                            elif (int(line[0]) in range(6, 10) and int(line[3]) in range(29, 46) and "-1" in line[
                                6]) or (int(line[0]) >= 10 and (int(line[3]) >= 46 or int(line[3]) < 29) and "-1" in
                                line[6]) or (
                                        int(line[0]) >= 10 and int(line[3]) in range(29, 46) and int(line[6]) in range(
                                    0, 15)):
                                p.append("0.75")
                                # print(line)
                            elif (int(line[0]) in range(6, 10) and (int(line[3]) >= 46 or int(line[3]) < 29) and "-1" in
                                line[6]) or (int(line[0]) in range(6, 10) and int(line[3]) in range(29, 46) and int(
                                    line[6]) in range(0, 15)) or (
                                        int(line[0]) >= 10 and (int(line[3]) >= 46 or int(line[3]) < 29) and int(
                                    line[6]) in range(0, 15)) or (int(line[0]) in range(6, 10) and (
                                    int(line[3]) >= 46 or int(line[3]) < 29) and int(line[6]) in range(0, 15)):
                                p.append("0.5")
                                # print(line)
                            else:
                                p.append("0.25")
                                # print(line)
                        elif ("2" in line[1] or "1" in line[1]):
                            if int(line[0]) >= 10 and int(line[3]) in range(29, 46) and "-1" in line[6]:
                                p.append("0.75")
                                # print(line)
                            elif (int(line[0]) in range(6, 10) and int(line[3]) in range(29, 46) and "-1" in line[
                                6]) or (int(line[0]) >= 10 and (int(line[3]) >= 46 or int(line[3]) < 29) and "-1" in
                                line[6]) or (
                                        int(line[0]) >= 10 and int(line[3]) in range(29, 46) and int(line[6]) in range(
                                    0, 15)):
                                p.append("0.5")
                                # print(line)
                            elif (int(line[0]) in range(6, 10) and (int(line[3]) >= 46 or int(line[3]) < 29) and "-1" in
                                line[6]) or (int(line[0]) in range(6, 10) and int(line[3]) in range(29, 46) and int(
                                    line[6]) in range(0, 15)) or (
                                        int(line[0]) >= 10 and (int(line[3]) >= 46 or int(line[3]) < 29) and int(
                                    line[6]) in range(0, 15)) or (int(line[0]) in range(6, 10) and (
                                    int(line[3]) >= 46 or int(line[3]) < 29) and int(line[6]) in range(0, 15)):
                                p.append("0.25")
                                # print(line)
                            else:
                                p.append("0.15")
                        elif "0" in line[1]:
                            if int(line[0]) >= 10 and int(line[3]) in range(29, 46) and "-1" in line[6]:
                                p.append("0.5")
                            elif (int(line[0]) in range(6, 10) and int(line[3]) in range(29, 46) and "-1" in line[
                                6]) or (int(line[0]) >= 10 and (int(line[3]) >= 46 or int(line[3]) < 29) and "-1" in
                                line[6]) or (
                                        int(line[0]) >= 10 and int(line[3]) in range(29, 46) and int(line[6]) in range(
                                    0, 15)):
                                p.append("0.25")
                            else:
                                p.append("0.15")
                    elif ("1" in line[4] and "1" in line[5]) or ("1" in line[4] and "2" in line[5]) or (
                            "2" in line[4] and "1" in line[5]):
                        if ("3" in line[1]):
                            if int(line[0]) >= 10 and int(line[3]) in range(29, 46) and "-1" in line[6]:
                                p.append("0.75")
                                # print(line)
                            elif (int(line[0]) in range(6, 10) and int(line[3]) in range(29, 46) and "-1" in line[
                                6]) or (int(line[0]) >= 10 and (int(line[3]) >= 46 or int(line[3]) < 29) and "-1" in
                                line[6]) or (
                                        int(line[0]) >= 10 and int(line[3]) in range(29, 46) and int(line[6]) in range(
                                    0, 15)):
                                p.append("0.5")
                                # print(line)
                            elif (int(line[0]) in range(6, 10) and (int(line[3]) >= 46 or int(line[3]) < 29) and "-1" in
                                line[6]) or (int(line[0]) in range(6, 10) and int(line[3]) in range(29, 46) and int(
                                    line[6]) in range(0, 15)) or (
                                        int(line[0]) >= 10 and (int(line[3]) >= 46 or int(line[3]) < 29) and int(
                                    line[6]) in range(0, 15)) or (int(line[0]) in range(6, 10) and (
                                    int(line[3]) >= 46 or int(line[3]) < 29) and int(line[6]) in range(0, 15)):
                                p.append("0.25")
                                # print(line)
                            else:
                                p.append("0.15")
                        elif ("2" in line[1] or "1" in line[1]):
                            if int(line[0]) >= 10 and int(line[3]) in range(29, 46) and "-1" in line[6]:
                                p.append("0.5")
                                # print(line)
                            elif (int(line[0]) in range(6, 10) and int(line[3]) in range(29, 46) and "-1" in line[
                                6]) or (int(line[0]) >= 10 and (int(line[3]) >= 46 or int(line[3]) < 29) and "-1" in
                                line[6]) or (
                                        int(line[0]) >= 10 and int(line[3]) in range(29, 46) and int(line[6]) in range(
                                    0, 15)):
                                p.append("0.25")
                                # print(line)
                            else:
                                p.append("0.15")
                        elif "0" in line[1]:
                            if int(line[0]) >= 10 and int(line[3]) in range(29, 46) and "-1" in line[6]:
                                p.append("0.25")
                            else:
                                p.append("0.15")
                        else:
                            print(line)
                    else:
                        print(line)

                else:
                    a = 9;

        with open(fout, "w") as output:
            output.write('\n'.join(p))

    def cb_sms_gtgen(self):
            # Ground Truth Generation
            finalSMSinp = 'C:\\Users\\tina\\Desktop\\finalSMSinp.csv'
            outfile = "C:\\Users\\tina\\Desktop\\SMSFinalGT.csv"

            p = list()

            with open(finalSMSinp, 'r+') as f:
                lines = f.readlines()
                for i in range(0, len(lines)):

                    line = lines[i].split(",")

                    # Checks for sent message
                    if line[2] == "0":
                        # Checks for foreign CC and fixed line Mobility. Also, it will check for local cc and fixed mobility, unknown cc and fixed mobility
                        if ("0" in line[5] and "0" in line[6]) or ("0" in line[5] and "1" in line[6]) or (
                                "2" in line[5] and "0" in line[6]) or ("1" in line[5] and "0" in line[6]) or (
                                "0" in line[5] and "2" in line[6]):
                            p.append("0.15")
                            # print(i)
                            # print(line)
                        # Checks patterns with cc=2 and mob=2
                        elif ("2" in line[5] and "2" in line[6]):
                            # Checks for suspicious sent
                            if "3" in line[1] and int(line[0]) > 60 and int(line[3]) in range(60, 120) and int(
                                    line[4]) in range(7, 70):
                                p.append("1")
                            # Checks for lower suspicion at night 0.15
                            elif ("3" in line[1] and int(line[0]) < 15) or ("3" in line[1] and int(line[3]) > 500) or (
                                    "3" in line[1] and int(line[4]) > 300):
                                p.append("0.25")
                            elif ("3" in line[1] and int(line[0]) > 60 and int(line[3]) in range(60, 120)) or (
                                        "3" in line[1] and int(line[0]) > 60 and int(line[4]) in range(7, 70)) or (
                                        "3" in line[1] and int(line[3]) in range(60, 120) and int(line[4]) in range(7,
                                                                                                                    70)) or (
                                            "3" in line[1] and int(line[0]) in range(40, 59) and int(line[3]) in range(
                                        60, 120) and int(line[4]) in range(7, 70)) or (
                                        "3" in line[1] and int(line[3]) in range(45, 59) and int(line[0]) > 60) or (
                                        "3" in line[1] and int(line[0]) in range(40, 59) and int(line[4]) in range(71,
                                                                                                                   95)):
                                p.append("0.75")
                            elif "3" in line[1]:
                                p.append("0.5")
                            elif ("2" in line[1] and int(line[1]) > 60) or (
                                    "2" in line[1] and int(line[3]) in range(60, 120)) or (
                                    "2" in line[1] and int(line[4]) in range(7, 70)):
                                p.append("0.5")
                            elif ("2" in line[1] and int(line[0]) < 15) or ("2" in line[1] and int(line[3]) > 500) or (
                                    "2" in line[1] and int(line[4]) > 300):
                                p.append("0.15")
                            elif "2" in line[1]:
                                p.append("0.25")
                            elif ("1" in line[1] and int(line[1]) > 60) or (
                                    "1" in line[1] and int(line[3]) in range(60, 120)) or (
                                    "1" in line[1] and int(line[4]) in range(7, 70)):
                                p.append("0.5")
                            elif ("1" in line[1] and int(line[0]) < 15) or ("2" in line[1] and int(line[3]) > 500) or (
                                    "2" in line[1] and int(line[4]) > 300):
                                p.append("0.15")
                            elif "1" in line[1]:
                                p.append("0.25")
                            elif ("0" in line[1] and int(line[1]) > 60) or (
                                    "1" in line[1] and int(line[3]) in range(60, 120)) or (
                                    "1" in line[1] and int(line[4]) in range(7, 70)):
                                p.append("0.25")
                            elif "0" in line[1]:
                                p.append("0.15")
                        elif ("1" in line[5] and "1" in line[6]) or ("1" in line[5] and "2" in line[6]) or (
                                "2" in line[5] and "1" in line[6]):
                            # Checks for suspicious sent
                            if "3" in line[1] and int(line[0]) > 60 and int(line[3]) in range(60, 120) and int(
                                    line[4]) in range(7,
                                                      70):
                                p.append("0.75")
                            # Checks for lower suspicion at night 0.15
                            elif ("3" in line[1] and int(line[0]) < 15) or ("3" in line[1] and int(line[3]) > 500) or (
                                            "3" in line[1] and int(line[4]) > 300):
                                p.append("0.15")
                            elif ("3" in line[1] and int(line[0]) > 60 and int(line[3]) in range(60, 120)) or (
                                                "3" in line[1] and int(line[0]) > 60 and int(line[4]) in range(7,
                                                                                                               70)) or (
                                                "3" in line[1] and int(line[3]) in range(60, 120) and int(
                                        line[4]) in range(7, 70)) or (
                                                    "3" in line[1] and int(line[0]) in range(40, 59) and int(
                                            line[3]) in range(60,
                                                              120) and int(
                                        line[4]) in range(7, 70)) or (
                                                "3" in line[1] and int(line[3]) in range(45, 59) and int(
                                        line[0]) > 60) or (
                                                "3" in line[1] and int(line[0]) in range(40, 59) and int(
                                        line[4]) in range(71, 95)):
                                p.append("0.5")
                            elif "3" in line[1]:
                                p.append("0.25")
                            elif ("2" in line[1] and int(line[1]) > 60) or (
                                    "2" in line[1] and int(line[3]) in range(60, 120)) or (
                                            "2" in line[1] and int(line[4]) in range(7, 70)):
                                p.append("0.25")
                            elif ("2" in line[1] and int(line[0]) < 15) or ("2" in line[1] and int(line[3]) > 500) or (
                                            "2" in line[1] and int(line[4]) > 300):
                                p.append("0.15")
                            elif "2" in line[1]:
                                p.append("0.25")
                            elif ("1" in line[1] and int(line[1]) > 60) or (
                                    "1" in line[1] and int(line[3]) in range(60, 120)) or (
                                            "1" in line[1] and int(line[4]) in range(7, 70)):
                                p.append("0.25")
                            elif ("1" in line[1] and int(line[0]) < 15) or ("2" in line[1] and int(line[3]) > 500) or (
                                            "2" in line[1] and int(line[4]) > 300):
                                p.append("0.15")
                            elif "1" in line[1]:
                                p.append("0.15")
                            elif ("0" in line[1] and int(line[1]) > 60) or (
                                    "1" in line[1] and int(line[3]) in range(60, 120)) or (
                                            "1" in line[1] and int(line[4]) in range(7, 70)):
                                p.append("0.15")
                            elif "0" in line[1]:
                                p.append("0.15")


                        else:
                            print(line)


                    # Checks for received message
                    elif line[2] == "1":
                        # Checks for foreign CC and fixed line Mobility. Also, it will check for local cc and fixed mobility unknown cc and fixed mobility
                        if ("0" in line[5] and "0" in line[6]) or ("0" in line[5] and "1" in line[6]) or (
                                "2" in line[5] and "0" in line[6]) or ("1" in line[5] and "0" in line[6]) or (
                                "0" in line[5] and "2" in line[6]):
                            p.append("0.15")
                            # print(i)
                            # print(line)
                        # Checks patterns with cc=2 and mob=2
                        elif ("2" in line[5] and "2" in line[6]):
                            # Checks for suspicious received
                            if "3" in line[1] and int(line[0]) > 60 and int(line[3]) in range(60, 120):
                                p.append("1")
                            # Checks for lower suspicion at night 0.15
                            elif ("3" in line[1] and int(line[0]) < 15) or ("3" in line[1] and int(line[3]) > 500) or (
                                    "3" in line[1] and int(line[4]) > 300):
                                p.append("0.25")
                            elif ("3" in line[1] and int(line[0]) > 60 and int(line[3]) in range(60, 120)) or (
                                        "3" in line[1] and int(line[0]) > 60 and int(line[4]) in range(7, 70)) or (
                                        "3" in line[1] and int(line[3]) in range(60, 120) and int(line[4]) in range(7,
                                                                                                                    70)) or (
                                            "3" in line[1] and int(line[0]) in range(40, 59) and int(line[3]) in range(
                                        60, 120) and int(line[4]) in range(7, 70)) or (
                                        "3" in line[1] and int(line[3]) in range(40, 59) and int(line[0]) > 60) or (
                                        "3" in line[1] and int(line[0]) in range(40, 59) and int(line[4]) in range(71,
                                                                                                                   95)):
                                p.append("0.75")
                            elif "3" in line[1]:
                                p.append("0.5")
                            elif ("2" in line[1] and int(line[1]) > 60) or (
                                    "2" in line[1] and int(line[3]) in range(60, 120)) or (
                                    "2" in line[1] and int(line[4]) in range(7, 70)):
                                p.append("0.5")
                            elif ("2" in line[1] and int(line[0]) < 15) or ("2" in line[1] and int(line[3]) > 500) or (
                                    "2" in line[1] and int(line[4]) > 300):
                                p.append("0.15")
                            elif "2" in line[1]:
                                p.append("0.25")
                            elif ("1" in line[1] and int(line[1]) > 60) or (
                                    "1" in line[1] and int(line[3]) in range(6, 15)) or (
                                    "1" in line[1] and int(line[4]) in range(7, 70)):
                                p.append("0.5")
                            elif ("1" in line[1] and int(line[0]) < 15) or ("2" in line[1] and int(line[3]) > 500) or (
                                    "2" in line[1] and int(line[4]) > 300):
                                p.append("0.15")
                            elif "1" in line[1]:
                                p.append("0.25")
                            elif ("0" in line[1] and int(line[1]) > 60) or (
                                    "1" in line[1] and int(line[3]) in range(6, 15)) or (
                                    "1" in line[1] and int(line[4]) in range(7, 70)):
                                p.append("0.25")
                            elif "0" in line[1]:
                                p.append("0.15")
                        elif ("1" in line[5] and "1" in line[6]) or ("1" in line[5] and "2" in line[6]) or (
                                        "2" in line[5] and "1" in line[6]):
                            if "3" in line[1] and int(line[0]) > 60 and int(line[3]) in range(6, 15):
                                p.append("0.75")
                            # Checks for lower suspicion at night 0.15
                            elif ("3" in line[1] and int(line[0]) < 15) or ("3" in line[1] and int(line[3]) > 500) or (
                                    "3" in line[1] and int(line[4]) > 300):
                                p.append("0.25")
                            elif ("3" in line[1] and int(line[0]) > 60 and int(line[3]) in range(6, 15)) or (
                                        "3" in line[1] and int(line[0]) > 60 and int(line[4]) in range(7, 70)) or (
                                        "3" in line[1] and int(line[3]) in range(6, 15) and int(line[4]) in range(7,
                                                                                                                  70)) or (
                                            "3" in line[1] and int(line[0]) in range(40, 59) and int(line[3]) in range(
                                        6, 15) and int(line[4]) in range(7, 70)) or (
                                        "3" in line[1] and int(line[3]) in range(15, 20) and int(line[0]) > 60) or (
                                        "3" in line[1] and int(line[0]) in range(40, 59) and int(line[4]) in range(71,
                                                                                                                   95)):
                                p.append("0.75")
                            elif "3" in line[1]:
                                p.append("0.5")
                            elif ("2" in line[1] and int(line[1]) > 60) or (
                                    "2" in line[1] and int(line[3]) in range(6, 15)) or (
                                    "2" in line[1] and int(line[4]) in range(7, 70)):
                                p.append("0.25")
                            elif ("2" in line[1] and int(line[0]) < 15) or ("2" in line[1] and int(line[3]) > 500) or (
                                    "2" in line[1] and int(line[4]) > 300):
                                p.append("0.15")
                            elif "2" in line[1]:
                                p.append("0.15")
                            elif ("1" in line[1] and int(line[1]) > 60) or (
                                    "1" in line[1] and int(line[3]) in range(6, 15)) or (
                                    "1" in line[1] and int(line[4]) in range(7, 70)):
                                p.append("0.25")
                            elif ("1" in line[1] and int(line[0]) < 15) or ("2" in line[1] and int(line[3]) > 500) or (
                                    "2" in line[1] and int(line[4]) > 300):
                                p.append("0.15")
                            elif "1" in line[1]:
                                p.append("0.15")
                            elif ("0" in line[1] and int(line[1]) > 60) or (
                                    "1" in line[1] and int(line[3]) in range(6, 15)) or (
                                    "1" in line[1] and int(line[4]) in range(7, 70)):
                                p.append("0.15")
                            elif "0" in line[1]:
                                p.append("0.15")



                        else:
                            print("lol")
                            print(i)



                    else:

                        print(i)
                        # print("katiden paei kala")

                # print('\n'.join(str(p)))
                with open(outfile, "w") as output:
                    #output.write("GT")
                    #output.write("\n")
                    output.write('\n'.join(p))

    #Neural Network for: Cyberbullying: Calls ------------------- COMPLETE
    def cb_calls_neuralnet(self):
        f1 = "C:\\Users\\tina\\Desktop\\Various\\Current Research\\finalCallsinp.csv"
        f2 = "C:\\Users\\tina\\Desktop\\Various\\Current Research\\Datasets\\Cyberbullying\\Calls\\samsout_dd_calls_gt.csv"
        trainalg = "trainoss"
        numneur = 13
        # subprocess.call('matlab -nosplash -wait -r  \'C:/Users/tina/Documents/MATLAB/smsNN.m\'')
        # C:\Users\tina\Documents\MATLAB\smsNN.m
        eng = matlab.engine.start_matlab()
        eng.bprop_cb_calls(f1, f2, trainalg, nargout=0)

        print("---------------------------------------")

    # Neural Network for: Cyberbullying: SMS ------------------- COMPLETE
    def cb_sms_neuralnet(self):
        #Here we can edit the NN parameters

        f1 = "C:\\Users\\tina\\Desktop\\finalSMSinp.csv"
        f2 = "C:\\Users\\tina\\Desktop\\SMSFinalGT.csv"
        trainalg="trainlm"
        numneur=13
        #subprocess.call('matlab -nosplash -wait -r  \'C:/Users/tina/Documents/MATLAB/smsNN.m\'')
        #C:\Users\tina\Documents\MATLAB\smsNN.m
        eng = matlab.engine.start_matlab()
        eng.bprop_cb_sms(f1,f2,trainalg, nargout=0)


        print ("---------------------------------------")

    # Performance Metrics for: Cyberbullying: Calls ------------------- COMPLETE
    def cb_calls_acc(self):
        no = "C:\\Users\\tina\\Desktop\\Current Research\\Datasets\\Cyberbullying\\Calls\\samsout_cb_calls.csv"
        gt="C:\\Users\\tina\\Desktop\\Current Research\\Datasets\\Cyberbullying\\Calls\\samsout_dd_calls_gt.csv"

        orIn = "C:\\Users\\tina\\Desktop\\Current Research\\Datasets\\Cyberbullying\\Calls\\orIn.csv"
        filenames = [no, gt]
        with open(orIn, 'w') as writer:
            readers = [open(filename) for filename in filenames]
            for lines in zip(*readers):
                print(','.join([line.strip() for line in lines]), file=writer)

        with open(orIn, newline='') as f:
            r = csv.reader(f)
            data = [line for line in r]
        with open(orIn, 'w', newline='') as f:
            w = csv.writer(f)
            w.writerow(["N0", 'GT'])
            w.writerow(["discrete", "discrete"])
            w.writerow(['', 'class'])
            w.writerows(data)

        accIn = Orange.data.Table(orIn)

        rfc = Orange.classification.RandomForestLearner()
        knn = Orange.classification.KNNLearner()
        svm = Orange.classification.SVMLearner()
        nb = Orange.classification.NaiveBayesLearner()

        res = Orange.evaluation.CrossValidation(accIn, [rfc], k=10)


        print("Accuracy :", Orange.evaluation.scoring.CA(res))
        print("AUC      :", Orange.evaluation.scoring.AUC(res))
        print("Precision:", Orange.evaluation.Precision(res, average="weighted"))
        print("Recall   :", Orange.evaluation.Recall(res, average="weighted"))
        print("F1       :", Orange.evaluation.F1(res, average="weighted"))

    def cb_sms_acc(self):

        no="C:\\Users\\tina\\Desktop\\Current Research\\Datasets\\Cyberbullying\\SMS\\samsout_cb_sms.csv"
        gt="C:\\Users\\tina\\Desktop\\SMSFinalGT.csv"

        orIn = "C:\\Users\\tina\\Desktop\\orIn.csv"
        orInp="C:\\Users\\tina\\Desktop\\orInp.csv"

        filenames = [no, gt]
        with open(orIn, 'w') as writer:
            readers = [open(filename) for filename in filenames]
            for lines in zip(*readers):
                print(','.join([line.strip() for line in lines]), file=writer)

        with open(orIn, newline='') as f:
            r = csv.reader(f)
            data = [line for line in r]
        with open(orIn, 'w', newline='') as f:
            w = csv.writer(f)
            w.writerow(["N0", 'GT'])
            w.writerow(["continuous","discrete"])
            w.writerow(['', 'class'])
            w.writerows(data)


        accIn = Orange.data.Table(orIn)


        rfc=Orange.classification.RandomForestLearner()
        knn=Orange.classification.KNNLearner()
        svm= Orange.classification.SVMLearner()
        nb=Orange.classification.NaiveBayesLearner()

        res = Orange.evaluation.CrossValidation(accIn, [rfc, knn, svm,nb], k=10)

        print("           |RFC         |kNN        |SVM        |Naive Bayes")
        print("Accuracy :", Orange.evaluation.scoring.CA(res))
        print("AUC      :", Orange.evaluation.scoring.AUC(res))
        print("Precision:", Orange.evaluation.Precision(res, average="micro"))
        print("Recall   :", Orange.evaluation.Recall(res, average="micro"))
        print("F1       :", Orange.evaluation.F1(res, average="micro"))

    # Ground truth generation for the calls of the drug dealing use case
    def dd_calls_gtgen(self):
        fin = "C:\\Users\\tina\\Desktop\\Current Research\\finalCallsinp.csv"
        fout = "C:\\Users\\tina\\Desktop\\Current Research\\Datasets\\Drug Dealing\\Calls\\samsout_dd_calls_gt.csv"
        p = []

        with open(fin, 'r+') as f:
            lines = f.readlines()
            for i in range(0, len(lines)):

                line = lines[i].split(",")

                # Checks for outgoing call
                if line[2] == "0":
                    # Checks for foreign CC and fixed line Mobility. Also, it will check for local cc and fixed mobility, unknown cc and fixed mobility
                    if ("0" in line[4] and "0" in line[5]) or ("0" in line[4] and "1" in line[5]) or (
                                    "2" in line[4] and "0" in line[5]) or ("1" in line[4] and "0" in line[5]) or (
                                    "0" in line[4] and "2" in line[5]):
                        p.append("0.15")
                        # print(line)
                    elif ("2" in line[4] and "2" in line[5]):
                        # Checks for suspicious sent messages
                        if ("3" in line[1]) or ("2" in line[1]):
                            if (int(line[0]) >= 12 and int(line[3]) in range(35, 180) and int(line[6]) in range(5, 45)):
                                p.append("1")
                                # print(line)
                            elif (int(line[0]) in range(6, 12) and int(line[3]) in range(35, 180) and int(
                                    line[6]) in range(
                                    5, 45)) or (int(line[0]) >= 12 and (
                                    int(line[3]) in range(25, 35) or int(line[3]) in range(180, 210)) and int(
                                line[6]) in range(5, 45)) \
                                    or (int(line[0]) >= 12 and int(line[3]) in range(35, 180) and (
                                            int(line[6]) in range(0, 6) or int(line[6]) in range(45, 60))):
                                p.append("0.75")
                                # print(line)
                            elif (int(line[0]) in range(6, 12) and (
                                    int(line[3]) in range(25, 35) or int(line[3]) in range(180, 210)) and int(
                                    line[6]) in range(
                                    5, 45)) or (int(line[0]) >= 12 and (
                                    int(line[3]) in range(25, 35) or int(line[3]) in range(180, 210)) and int(
                                line[6]) in range(5, 45)) \
                                    or (int(line[0]) in range(6, 12) and int(line[3]) in range(35, 180) and (
                                            int(line[6]) in range(0, 6) or int(line[6]) in range(45, 60))
                                        or (int(line[0]) in range(6, 12) and int(line[3]) in range(25, 35) and (
                                                int(line[6]) in range(0, 6) or int(line[6]) in range(45, 60)))):
                                p.append("0.5")
                                # print(line)
                            else:
                                p.append("0.25")
                                # print(line)
                        elif ("1" in line[1]):
                            if (int(line[0]) >= 12 and int(line[3]) in range(35, 180) and int(line[6]) in range(5, 45)):
                                p.append("0.75")
                            elif (int(line[0]) in range(6, 12) and int(line[3]) in range(35, 180) and int(
                                    line[6]) in range(5, 45)) or (int(line[0]) >= 12 and (
                                    int(line[3]) in range(25, 35) or int(line[3]) in range(180, 210)) and int(
                                    line[6]) in range(5, 45)) \
                                    or (int(line[0]) >= 12 and int(line[3]) in range(35, 180) and (
                                            int(line[6]) in range(0, 6) or int(line[6]) in range(45, 60))):
                                p.append("0.5")
                                # print(line)
                            elif (int(line[0]) in range(6, 12) and (
                                            int(line[3]) in range(25, 35) or int(line[3]) in range(180, 210)) and int(
                                line[6]) in range(
                                5, 45)) or (int(line[0]) >= 12 and (
                                            int(line[3]) in range(25, 35) or int(line[3]) in range(180, 210)) and int(
                                line[6]) in range(5,
                                                  45)) \
                                    or (int(line[0]) in range(6, 12) and int(line[3]) in range(35, 180) and (
                                                    int(line[6]) in range(0, 6) or int(line[6]) in range(45, 60))
                                        or (int(line[0]) in range(6, 12) and int(line[3]) in range(25, 35) and (
                                                        int(line[6]) in range(0, 6) or int(line[6]) in range(45, 60)))):
                                p.append("0.25")
                                # print(line)
                            else:
                                p.append("0.15")
                                # print(line)
                        elif ("0" in line[1]):
                            if (int(line[0]) >= 12 and int(line[3]) in range(35, 180) and int(line[6]) in range(5, 45)):
                                p.append("0.5")
                                # print(line)
                            elif (int(line[0]) in range(6, 12) and int(line[3]) in range(35, 180) and int(
                                    line[6]) in range(5, 45)) or (int(line[0]) >= 12 and (
                                    int(line[3]) in range(25, 35) or int(line[3]) in range(180, 210)) and int(
                                    line[6]) in range(5, 45)) \
                                    or (int(line[0]) >= 12 and int(line[3]) in range(35, 180) and (
                                            int(line[6]) in range(0, 6) or int(line[6]) in range(45, 60))):
                                p.append("0.25")
                                # print(line)
                            else:
                                p.append("0.15")
                                # print(line)
                        else:
                            lol = 9
                            # print(line)
                    elif ("1" in line[4] and "1" in line[5]) or ("1" in line[4] and "2" in line[5]) or (
                            "2" in line[4] and "1" in line[5]):
                        if ("3" in line[1]) or ("2" in line[1]):
                            if (int(line[0]) >= 12 and int(line[3]) in range(35, 180) and int(line[6]) in range(5, 45)):
                                p.append("0.75")
                                # print(line)
                            elif (int(line[0]) in range(6, 12) and int(line[3]) in range(35, 180) and int(
                                    line[6]) in range(5, 45)) or (int(line[0]) >= 12 and (
                                    int(line[3]) in range(25, 35) or int(line[3]) in range(180, 210)) and int(
                                    line[6]) in range(5, 45)) \
                                    or (int(line[0]) >= 12 and int(line[3]) in range(35, 180) and (
                                            int(line[6]) in range(0, 6) or int(line[6]) in range(45, 60))):
                                p.append("0.5")
                                # print(line)
                            elif (int(line[0]) in range(6, 12) and (
                                    int(line[3]) in range(25, 35) or int(line[3]) in range(180, 210)) and int(
                                    line[6]) in range(
                                    5, 45)) or (int(line[0]) >= 12 and (
                                    int(line[3]) in range(25, 35) or int(line[3]) in range(180, 210)) and int(
                                line[6]) in range(5, 45)) \
                                    or (int(line[0]) in range(6, 12) and int(line[3]) in range(35, 180) and (
                                            int(line[6]) in range(0, 6) or int(line[6]) in range(45, 60))
                                        or (int(line[0]) in range(6, 12) and int(line[3]) in range(25, 35) and (
                                                int(line[6]) in range(0, 6) or int(line[6]) in range(45, 60)))):
                                p.append("0.25")
                                # print(line)
                            else:
                                p.append("0.15")
                        elif ("1" in line[1]) or ("0" in line[1]):
                            if (int(line[0]) >= 12 and int(line[3]) in range(35, 180) and int(line[6]) in range(5, 45)):
                                p.append("0.5")
                                # print(line)
                            elif (int(line[0]) in range(6, 12) and int(line[3]) in range(35, 180) and int(
                                    line[6]) in range(5, 45)) or (int(line[0]) >= 12 and (
                                    int(line[3]) in range(25, 35) or int(line[3]) in range(180, 210)) and int(
                                    line[6]) in range(5, 45)) \
                                    or (int(line[0]) >= 12 and int(line[3]) in range(35, 180) and (
                                            int(line[6]) in range(0, 6) or int(line[6]) in range(45, 60))):
                                p.append("0.25")
                                # print(line)
                            else:
                                p.append("0.15")
                                # print(line)
                        else:
                            lol = 4
                            # print(line)
                    else:
                        lol = 99
                        # print(line)
                elif line[2] == "1":
                    # Checks for foreign CC and fixed line Mobility. Also, it will check for local cc and fixed mobility, unknown cc and fixed mobility
                    if ("0" in line[4] and "0" in line[5]) or ("0" in line[4] and "1" in line[5]) or (
                                    "2" in line[4] and "0" in line[5]) or ("1" in line[4] and "0" in line[5]) or (
                                    "0" in line[4] and "2" in line[5]):
                        p.append("0.15")
                        # print(line)
                    elif ("2" in line[4] and "2" in line[5]):
                        # Checks for suspicious sent messages
                        if ("3" in line[1]) or ("2" in line[1]):
                            if (int(line[0]) >= 12 and int(line[3]) in range(35, 180) and (
                                    int(line[6]) in range(5, 45) or "-1" in line[6])):
                                p.append("1")
                                # print(line)
                            elif (int(line[0]) in range(6, 12) and int(line[3]) in range(35, 180) and (
                                    int(line[6]) in range(
                                        5, 45) or "-1" in line[6])) or (int(line[0]) >= 12 and (
                                    int(line[3]) in range(25, 35) or int(line[3]) in range(180, 210)) and (
                                    int(line[6]) in range(5, 45) or "-1" in line[6])) \
                                    or (int(line[0]) >= 12 and int(line[3]) in range(35, 180) and (
                                            int(line[6]) in range(0, 6) or int(line[6]) in range(45, 60))):
                                p.append("0.75")
                                # print(line)
                            elif (int(line[0]) in range(6, 12) and (
                                    int(line[3]) in range(25, 35) or int(line[3]) in range(180, 210)) and (
                                    int(line[6]) in range(
                                        5, 45) or "-1" in line[6])) or (int(line[0]) >= 12 and (
                                    int(line[3]) in range(25, 35) or int(line[3]) in range(180, 210)) and (
                                    int(line[6]) in range(
                                    5, 45) or "-1" in line[6])) \
                                    or (int(line[0]) in range(6, 12) and int(line[3]) in range(35, 180) and (
                                            int(line[6]) in range(0, 6) or int(line[6]) in range(45, 60))
                                        or (int(line[0]) in range(6, 12) and int(line[3]) in range(25, 35) and (
                                                int(line[6]) in range(0, 6) or int(line[6]) in range(45, 60)))):
                                p.append("0.5")
                                # print(line)
                            else:
                                p.append("0.25")
                                # print(line)
                        elif ("1" in line[1]):
                            if int(line[0]) >= 12 and int(line[3]) in range(35, 180) and (
                                    int(line[6]) in range(5, 45) or "-1" in line[6]):
                                p.append("0.75")
                                # print(line)
                            elif (int(line[0]) in range(6, 12) and int(line[3]) in range(35, 180) and (
                                    int(line[6]) in range(
                                        5, 45) or "-1" in line[6])) or (int(line[0]) >= 12 and (
                                    int(line[3]) in range(25, 35) or int(line[3]) in range(180, 210)) and (
                                    int(line[6]) in range(5, 45) or "-1" in line[6])) \
                                    or (int(line[0]) >= 12 and int(line[3]) in range(35, 180) and (
                                            int(line[6]) in range(0, 6) or int(line[6]) in range(45, 60))):
                                p.append("0.5")
                                # print(line)
                            elif (int(line[0]) in range(6, 12) and (
                                    int(line[3]) in range(25, 35) or int(line[3]) in range(180, 210)) and (
                                    int(line[6]) in range(
                                        5, 45) or "-1" in line[6])) or (int(line[0]) >= 12 and (
                                    int(line[3]) in range(25, 35) or int(line[3]) in range(180, 210)) and (
                                    int(line[6]) in range(
                                    5, 45) or "-1" in line[6])) \
                                    or (int(line[0]) in range(6, 12) and int(line[3]) in range(35, 180) and (
                                            int(line[6]) in range(0, 6) or int(line[6]) in range(45, 60))
                                        or (int(line[0]) in range(6, 12) and int(line[3]) in range(25, 35) and (
                                                int(line[6]) in range(0, 6) or int(line[6]) in range(45, 60)))):
                                p.append("0.25")
                                # print(line)
                            else:
                                p.append("0.15")
                                # print(line)
                        elif ("0" in line[1]):
                            if int(line[0]) >= 12 and int(line[3]) in range(35, 180) and (
                                    int(line[6]) in range(5, 45) or "-1" in line[6]):
                                p.append("0.5")
                                # print(line)
                            elif (int(line[0]) in range(6, 12) and int(line[3]) in range(35, 180) and (
                                    int(line[6]) in range(
                                        5, 45) or "-1" in line[6])) or (int(line[0]) >= 12 and (
                                    int(line[3]) in range(25, 35) or int(line[3]) in range(180, 210)) and (
                                    int(line[6]) in range(5, 45) or "-1" in line[6])) \
                                    or (int(line[0]) >= 12 and int(line[3]) in range(35, 180) and (
                                            int(line[6]) in range(0, 6) or int(line[6]) in range(45, 60))):
                                p.append("0.25")
                                # print(line)
                            else:
                                p.append("0.15")
                                # print(line)
                        else:
                            lol = 3
                            # print(line)
                    elif ("1" in line[4] and "1" in line[5]) or ("1" in line[4] and "2" in line[5]) or (
                            "2" in line[4] and "1" in line[5]):
                        if ("3" in line[1]) or ("2" in line[1]):
                            if (int(line[0]) >= 12 and int(line[3]) in range(35, 180) and (
                                            int(line[6]) in range(5, 45) or "-1" in line[6])):
                                p.append("0.75")
                                # print(line)
                            elif (int(line[0]) in range(6, 12) and int(line[3]) in range(35, 180) and (
                                    int(line[6]) in range(
                                        5, 45) or "-1" in line[6])) or (int(line[0]) >= 12 and (
                                            int(line[3]) in range(25, 35) or int(line[3]) in range(180, 210)) and (
                                            int(line[6]) in range(5, 45) or "-1" in line[6])) \
                                    or (int(line[0]) >= 12 and int(line[3]) in range(35, 180) and (
                                                    int(line[6]) in range(0, 6) or int(line[6]) in range(45, 60))):
                                p.append("0.5")
                                # print(line)
                            elif (int(line[0]) in range(6, 12) and (
                                    int(line[3]) in range(25, 35) or int(line[3]) in range(180, 210)) and (
                                    int(line[6]) in range(
                                        5, 45) or "-1" in line[6])) or (int(line[0]) >= 12 and (
                                    int(line[3]) in range(25, 35) or int(line[3]) in range(180, 210)) and (
                                    int(line[6]) in range(
                                    5, 45) or "-1" in line[6])) \
                                    or (int(line[0]) in range(6, 12) and int(line[3]) in range(35, 180) and (
                                            int(line[6]) in range(0, 6) or int(line[6]) in range(45, 60))
                                        or (int(line[0]) in range(6, 12) and int(line[3]) in range(25, 35) and (
                                                int(line[6]) in range(0, 6) or int(line[6]) in range(45, 60)))):
                                p.append("0.25")
                                # print(line)
                            else:
                                p.append("0.15")
                                # print(line)
                        elif ("1" in line[1]) or ("0" in line[1]):
                            if (int(line[0]) >= 12 and int(line[3]) in range(35, 180) and (
                                            int(line[6]) in range(5, 45) or "-1" in line[6])):
                                p.append("0.5")
                                # print(line)
                            elif (int(line[0]) in range(6, 12) and int(line[3]) in range(35, 180) and (
                                    int(line[6]) in range(
                                        5, 45) or "-1" in line[6])) or (int(line[0]) >= 12 and (
                                            int(line[3]) in range(25, 35) or int(line[3]) in range(180, 210)) and (
                                            int(line[6]) in range(5, 45) or "-1" in line[6])) \
                                    or (int(line[0]) >= 12 and int(line[3]) in range(35, 180) and (
                                                    int(line[6]) in range(0, 6) or int(line[6]) in range(45, 60))):
                                p.append("0.25")
                                # print(line)
                            else:
                                p.append("0.15")
                                # print(line)
                        else:
                            lol = 98
                    else:
                        q = 1

                else:
                    # print(line)
                    lol = 1

        with open(fout, "w") as output:
            output.write('\n'.join(p))

    #Ground truth generation for the SMS of the drug dealing use case
    def dd_sms_gtgen(self):
        fin = "C:\\Users\\tina\\Desktop\\finalSMSinp.csv"
        fout = "C:\\Users\\tina\\Desktop\\finalSMSGTdrugs.csv"
        p = []

        with open(fin, 'r+') as f:
            lines = f.readlines()
            for i in range(0, len(lines)):

                line = lines[i].split(",")

                # Checks for sent message
                if line[2] == "0":
                    # Checks for foreign CC and fixed line Mobility. Also, it will check for local cc and fixed mobility, unknown cc and fixed mobility
                    if ("0" in line[5] and "0" in line[6]) or ("0" in line[5] and "1" in line[6]) or (
                                    "2" in line[5] and "0" in line[6]) or ("1" in line[5] and "0" in line[6]) or (
                                    "0" in line[5] and "2" in line[6]):
                        p.append("0.15")
                    # Checks patterns with cc=2 and mob=2
                    elif ("2" in line[5] and "2" in line[6]):
                        # Checks for suspicious sent messages
                        if ("3" in line[1]) or ("2" in line[1]):
                            if (int(line[0]) >= 60 and int(line[3]) in range(120, 180) and int(line[4]) in range(40,
                                                                                                                 190)):
                                p.append("1")
                                # print(line)
                            elif (int(line[0]) in range(35, 60) and int(line[3]) in range(120, 180) and int(
                                    line[4]) in range(40, 190)) or (
                                        int(line[0]) >= 60 and int(line[3]) >= 180 and int(line[4]) in range(40, 190)) \
                                    or (int(line[0]) >= 60 and int(line[3]) in range(120, 180) and int(
                                        line[4]) in range(20, 40)):
                                p.append("0.75")
                                # print(line)
                            elif (int(line[0]) in range(35, 60) and int(line[3]) >= 180 and int(line[4]) in range(40,
                                                                                                                  190)) or \
                                    (int(line[0]) in range(35, 60) and int(line[3]) in range(120, 180) and int(
                                        line[4]) in range(20, 40)) or \
                                    (int(line[0]) >= 60 and int(line[3]) in range(120, 180) and int(line[4]) in range(
                                        20, 40)):
                                p.append("0.5")
                                # print(line)
                            else:
                                p.append("0.25")
                        elif ("1" in line[1]):
                            if (int(line[0]) >= 60 and int(line[3]) in range(120, 180) and int(line[4]) in range(40,
                                                                                                                 190)):
                                p.append("0.75")
                                # print(line)
                            elif (int(line[0]) in range(35, 60) and int(line[3]) in range(120, 180) and int(
                                    line[4]) in range(
                                    40, 190)) or (
                                        int(line[0]) >= 60 and int(line[3]) > 180 and int(line[4]) in range(40, 190)) \
                                    or (int(line[0]) >= 60 and int(line[3]) in range(120, 180) and int(
                                        line[4]) in range(20, 40)):
                                p.append("0.5")
                                # print(line)
                            elif (int(line[0]) in range(35, 60) and int(line[3]) > 180 and int(line[4]) in range(40,
                                                                                                                 190)) or \
                                    (int(line[0]) in range(35, 60) and int(line[3]) in range(120, 180) and int(
                                        line[4]) in range(20, 40)) or \
                                    (int(line[0]) >= 60 and int(line[3]) in range(120, 180) and int(line[4]) in range(
                                        20, 40)):
                                p.append("0.25")
                                # print(line)
                            else:
                                p.append("0.15")
                                # print(line)
                        elif ("0" in line[1]):
                            if (int(line[0]) >= 60 and int(line[3]) in range(120, 180) and int(line[4]) in range(40,
                                                                                                                 190)):
                                p.append("0.5")
                                # print(line)
                            elif (int(line[0]) in range(35, 60) and int(line[3]) in range(120, 180) and int(
                                    line[4]) in range(
                                    40, 190)) or (
                                        int(line[0]) >= 60 and int(line[3]) > 180 and int(line[4]) in range(40, 190)) \
                                    or (int(line[0]) >= 60 and int(line[3]) in range(120, 180) and int(
                                        line[4]) in range(20, 40)):
                                p.append("0.25")
                                # print(line)
                            else:
                                p.append("0.15")
                                # print(line)
                        else:
                            # print(line)
                            a = 4
                    elif ("1" in line[5] and "1" in line[6]) or ("1" in line[5] and "2" in line[6]) or (
                            "2" in line[5] and "1" in line[6]):
                        if ("3" in line[1]) or ("2" in line[1]):
                            if (int(line[0]) >= 60 and int(line[3]) in range(120, 180) and int(line[4]) in range(40,
                                                                                                                 190)):
                                p.append("0.75")
                                # print(line)
                            elif (int(line[0]) in range(35, 60) and int(line[3]) in range(120, 180) and int(
                                    line[4]) in range(40,
                                                      190)) or (
                                                int(line[0]) >= 60 and int(line[3]) > 180 and int(line[4]) in range(40,
                                                                                                                    190)) \
                                    or (int(line[0]) >= 60 and int(line[3]) in range(120, 180) and int(
                                        line[4]) in range(20, 40)):
                                p.append("0.5")
                                # print(line)
                            elif (int(line[0]) in range(35, 60) and int(line[3]) > 180 and int(line[4]) in range(40,
                                                                                                                 190)) or \
                                    (int(line[0]) in range(35, 60) and int(line[3]) in range(120, 180) and int(
                                        line[4]) in range(20,
                                                          40)) or \
                                    (int(line[0]) >= 60 and int(line[3]) in range(120, 180) and int(line[4]) in range(
                                        20, 40)):
                                p.append("0.25")
                                # print(line)
                            else:
                                p.append("0.15")
                        elif "1" in line[1]:
                            if (int(line[0]) >= 60 and int(line[3]) in range(120, 180) and int(line[4]) in range(40,
                                                                                                                 190)):
                                p.append("0.5")
                                # print(line)
                            elif (int(line[0]) in range(35, 60) and int(line[3]) in range(120, 180) and int(
                                    line[4]) in range(
                                40, 190)) or (
                                                int(line[0]) >= 60 and int(line[3]) > 180 and int(line[4]) in range(40,
                                                                                                                    190)) \
                                    or (int(line[0]) >= 60 and int(line[3]) in range(120, 180) and int(
                                        line[4]) in range(20,
                                                          40)):
                                p.append("0.25")
                                # print(line)
                            elif (int(line[0]) in range(35, 60) and int(line[3]) > 180 and int(line[4]) in range(40,
                                                                                                                 190)) or \
                                    (int(line[0]) in range(35, 60) and int(line[3]) in range(120, 180) and int(
                                        line[4]) in range(20, 40)) or \
                                    (int(line[0]) >= 60 and int(line[3]) in range(120, 180) and int(line[4]) in range(
                                        20,
                                        40)):
                                p.append("0.15")
                                # print(line)
                            else:
                                p.append("0.15")
                                # print(line)
                        elif ("0" in line[1]):
                            if (int(line[0]) >= 60 and int(line[3]) in range(120, 180) and int(line[4]) in range(40,
                                                                                                                 190)):
                                p.append("0.5")
                                # print(line)
                            elif (int(line[0]) in range(35, 60) and int(line[3]) in range(120, 180) and int(
                                    line[4]) in range(
                                    40, 190)) or (
                                        int(line[0]) >= 60 and int(line[3]) > 180 and int(line[4]) in range(40, 190)) \
                                    or (int(line[0]) >= 60 and int(line[3]) in range(120, 180) and int(
                                        line[4]) in range(20, 40)):
                                p.append("0.25")
                                # print(line)
                            else:
                                p.append("0.15")
                                # print(line)
                        else:
                            # print(line)
                            q = 3
                    else:
                        # print(line)
                        o = 7
                else:  # if it is an incoming message
                    # print(line)
                    if ("0" in line[5] and "0" in line[6]) or ("0" in line[5] and "1" in line[6]) or (
                                    "2" in line[5] and "0" in line[6]) or ("1" in line[5] and "0" in line[6]) or (
                                    "0" in line[5] and "2" in line[6]):
                        p.append("0.15")
                        # print(line)
                    elif ("2" in line[5] and "2" in line[6]):
                        # Checks for suspicious sent messages
                        if ("3" in line[1]) or ("2" in line[1]):
                            if (int(line[0]) >= 60 and int(line[3]) in range(120, 180) and int(line[4]) in range(15,
                                                                                                                 100)):
                                p.append("1")
                                print(line)
                            elif (int(line[0]) in range(35, 60) and int(line[3]) in range(120, 180) and int(
                                    line[4]) in range(15, 100)) or (
                                        int(line[0]) >= 60 and int(line[3]) >= 180 and int(line[4]) in range(15, 100)) \
                                    or (int(line[0]) >= 60 and int(line[3]) in range(120, 180) and int(
                                        line[4]) in range(100, 250)):
                                p.append("0.75")
                                # print(line)
                            elif (int(line[0]) in range(35, 60) and int(line[3]) >= 180 and int(line[4]) in range(15,
                                                                                                                  100)) or \
                                    (int(line[0]) in range(35, 60) and int(line[3]) in range(120, 180) and int(
                                        line[4]) in range(100, 250)) or \
                                    (int(line[0]) >= 60 and int(line[3]) >= 180 and int(line[4]) in range(100, 250)):
                                p.append("0.5")
                                # print(line)
                            else:
                                p.append("0.25")
                        elif ("1" in line[1]):
                            if (int(line[0]) >= 60 and int(line[3]) in range(120, 180) and int(line[4]) in range(15,
                                                                                                                 100)):
                                p.append("0.75")
                                # print(line)
                            elif (int(line[0]) in range(35, 60) and int(line[3]) in range(120, 180) and int(
                                    line[4]) in range(
                                    15, 100)) or (
                                        int(line[0]) >= 60 and int(line[3]) >= 180 and int(line[4]) in range(15, 100)) \
                                    or (int(line[0]) >= 60 and int(line[3]) in range(120, 180) and int(
                                        line[4]) in range(100, 250)):
                                p.append("0.5")
                                # print(line)
                            elif (int(line[0]) in range(35, 60) and int(line[3]) >= 180 and int(line[4]) in range(15,
                                                                                                                  100)) or \
                                    (int(line[0]) in range(35, 60) and int(line[3]) in range(120, 180) and int(
                                        line[4]) in range(100, 250)) or \
                                    (int(line[0]) >= 60 and int(line[3]) in range(120, 180) and int(line[4]) in range(
                                        100, 250)):
                                p.append("0.25")
                                # print(line)
                            else:
                                p.append("0.15")
                                # print(line)
                        elif ("0" in line[1]):
                            if (int(line[0]) >= 60 and int(line[3]) in range(120, 180) and int(line[4]) in range(15,
                                                                                                                 100)):
                                p.append("0.5")
                                # print(line)
                            elif (int(line[0]) in range(35, 60) and int(line[3]) in range(120, 180) and int(
                                    line[4]) in range(
                                    15, 100)) or (
                                        int(line[0]) >= 60 and int(line[3]) >= 180 and int(line[4]) in range(15, 100)) \
                                    or (int(line[0]) >= 60 and int(line[3]) in range(120, 180) and int(
                                        line[4]) in range(100, 250)):
                                p.append("0.25")
                                # print(line)
                            else:
                                p.append("0.15")
                                # print(line)
                        else:
                            # print(line)
                            a = 4
                    elif ("1" in line[5] and "1" in line[6]) or ("1" in line[5] and "2" in line[6]) or (
                            "2" in line[5] and "1" in line[6]):
                        if ("3" in line[1]) or ("2" in line[1]):
                            if (int(line[0]) >= 60 and int(line[3]) in range(120, 180) and int(line[4]) in range(15,
                                                                                                                 100)):
                                p.append("0.75")
                                # print(line)
                            elif (int(line[0]) in range(35, 60) and int(line[3]) in range(120, 180) and int(
                                    line[4]) in range(15, 100)) or (
                                                int(line[0]) >= 60 and int(line[3]) >= 180 and int(line[4]) in range(15,
                                                                                                                     100)) \
                                    or (int(line[0]) >= 60 and int(line[3]) in range(120, 180) and int(
                                        line[4]) in range(100, 250)):
                                p.append("0.5")
                                # print(line)
                            elif (int(line[0]) in range(35, 60) and int(line[3]) >= 180 and int(line[4]) in range(15,
                                                                                                                  100)) or \
                                    (int(line[0]) in range(35, 60) and int(line[3]) in range(120, 180) and int(
                                        line[4]) in range(100, 250)) or \
                                    (int(line[0]) >= 60 and int(line[3]) in range(120, 180) and int(line[4]) in range(
                                        100, 250)):
                                p.append("0.25")
                                # print(line)
                            else:
                                p.append("0.15")
                        elif "1" in line[1]:
                            if (int(line[0]) >= 60 and int(line[3]) in range(120, 180) and int(line[4]) in range(15,
                                                                                                                 100)):
                                p.append("0.5")
                                # print(line)
                            elif (int(line[0]) in range(35, 60) and int(line[3]) in range(120, 180) and int(
                                    line[4]) in range(
                                15, 100)) or (
                                                int(line[0]) >= 60 and int(line[3]) >= 180 and int(line[4]) in range(15,
                                                                                                                     100)) \
                                    or (int(line[0]) >= 60 and int(line[3]) in range(120, 180) and int(
                                        line[4]) in range(100, 250)):
                                p.append("0.25")
                                # print(line)
                            elif (int(line[0]) in range(35, 60) and int(line[3]) >= 180 and int(line[4]) in range(15,
                                                                                                                  100)) or \
                                    (int(line[0]) in range(35, 60) and int(line[3]) in range(120, 180) and int(
                                        line[4]) in range(100, 250)) or \
                                    (int(line[0]) >= 60 and int(line[3]) in range(120, 180) and int(line[4]) in range(
                                        100, 250)):
                                p.append("0.15")
                                # print(line)
                            else:
                                p.append("0.15")
                                # print(line)
                        elif ("0" in line[1]):  # correct
                            if (int(line[0]) >= 60 and int(line[3]) in range(120, 180) and int(line[4]) in range(15,
                                                                                                                 100)):
                                p.append("0.5")
                                # print(line)
                            elif (int(line[0]) in range(35, 60) and int(line[3]) in range(120, 180) and int(
                                    line[4]) in range(
                                    15, 100)) or (
                                        int(line[0]) >= 60 and int(line[3]) >= 180 and int(line[4]) in range(15, 100)) \
                                    or (int(line[0]) >= 60 and int(line[3]) in range(120, 180) and int(
                                        line[4]) in range(100, 250)):
                                p.append("0.25")
                                # print(line)
                            else:
                                p.append("0.15")
                                # print(line)
                        else:
                            # print(line)
                            q = 3
                    else:
                        # print(line)
                        ppp = 3

            with open(fout, "w") as output:
                output.write('\n'.join(p))


                # elif ("3" in line[1] and int(line[0]) < 15) or ("3" in line[1] and int(line[3]) > 500) or (
                # "3" in line[1] and int(line[4]) > 300):
                # p.append("0.25")

    #Neural network testing for the salls, drug dealing use case
    def dd_calls_neuralnet(self):
        f1 = "C:\\Users\\tina\\Desktop\\Current Research\\finalCallsinp.csv"
        f2 = "C:\\Users\\tina\\Desktop\\Current Research\\Datasets\\Drug Dealing\\Calls\\samsout_dd_calls_gt.csv"
        trainalg='trainlm'
        eng = matlab.engine.start_matlab()
        eng.bprop_drugs_calls(f1, f2, trainalg, nargout=0)
        print("---------------------------------------")

    #Neural network testing for the SMS, drug dealing use case
    def dd_sms_neuralnet(self):
        f1 = "C:\\Users\\tina\\Desktop\\finalSMSinp.csv"
        f2 = "C:\\Users\\tina\\Desktop\\finalSMSGTdrugs.csv"
        trainalg = "trainlm"
        eng = matlab.engine.start_matlab()
        eng.bprop_drugs_sms(f1, f2, trainalg, nargout=0)
        print ("---------------------------------------")

    def dd_calls_acc(self):
        no="C:\\Users\\tina\\Desktop\\Current Research\\Datasets\\Drug Dealing\\Calls\\samsout_dd_calls.csv"
        gt="C:\\Users\\tina\\Desktop\\Current Research\\Datasets\\Drug Dealing\\Calls\\samsout_dd_calls_gt.csv"
        orIn = "C:\\Users\\tina\\Desktop\\drugCallsorIn.csv"

        filenames = [no, gt]
        with open(orIn, 'w') as writer:
            readers = [open(filename) for filename in filenames]
            for lines in zip(*readers):
                print(','.join([line.strip() for line in lines]), file=writer)

        with open(orIn, newline='') as f:
            r = csv.reader(f)
            data = [line for line in r]
        with open(orIn, 'w', newline='') as f:
            w = csv.writer(f)
            w.writerow(["N0", 'GT'])
            w.writerow(["continuous", "discrete"])
            w.writerow(['', 'class'])
            w.writerows(data)

        accIn = Orange.data.Table(orIn)

        rfc = Orange.classification.RandomForestLearner()
        knn = Orange.classification.KNNLearner()
        svm = Orange.classification.SVMLearner()
        nb = Orange.classification.NaiveBayesLearner()

        res = Orange.evaluation.CrossValidation(accIn, [rfc, knn, svm, nb], k=10)

        print("           |RFC         |kNN        |SVM        |Naive Bayes")
        print("Accuracy :", Orange.evaluation.scoring.CA(res))
        print("AUC      :", Orange.evaluation.scoring.AUC(res))
        print("Precision:", Orange.evaluation.Precision(res, average="micro"))
        print("Recall   :", Orange.evaluation.Recall(res, average="micro"))
        print("F1       :", Orange.evaluation.F1(res, average="micro"))
        print("---------------------------------------")

    #Accuracy calculation for the SMS drug dealing use case
    def dd_sms_acc(self):
        no = "C:\\Users\\tina\\Desktop\\Current Research\\Datasets\\Drug Dealing\\SMS\\samsout_drugs_sms.csv"
        gt = "C:\\Users\\tina\\Desktop\\finalSMSGTdrugs.csv"

        orIn = "C:\\Users\\tina\\Desktop\\drugorIn.csv"
        orInp = "C:\\Users\\tina\\Desktop\\orInp.csv"

        filenames = [no, gt]
        with open(orIn, 'w') as writer:
            readers = [open(filename) for filename in filenames]
            for lines in zip(*readers):
                print(','.join([line.strip() for line in lines]), file=writer)

        with open(orIn, newline='') as f:
            r = csv.reader(f)
            data = [line for line in r]
        with open(orIn, 'w', newline='') as f:
            w = csv.writer(f)
            w.writerow(["N0", 'GT'])
            w.writerow(["continuous", "discrete"])
            w.writerow(['', 'class'])
            w.writerows(data)

        accIn = Orange.data.Table(orIn)

        rfc = Orange.classification.RandomForestLearner()
        knn = Orange.classification.KNNLearner()
        svm = Orange.classification.SVMLearner()
        nb = Orange.classification.NaiveBayesLearner()

        res = Orange.evaluation.CrossValidation(accIn, [rfc, knn, svm, nb], k=10)

        print("           |RFC         |kNN        |SVM        |Naive Bayes")
        print("Accuracy :", Orange.evaluation.scoring.CA(res))
        print("AUC      :", Orange.evaluation.scoring.AUC(res))
        print("Precision:", Orange.evaluation.Precision(res, average="macro"))
        print("Recall   :", Orange.evaluation.Recall(res, average="macro"))
        print("F1       :", Orange.evaluation.F1(res, average="macro"))
        print("---------------------------------------")


if __name__== "__main__":
    load_intro()
    Menu().run()
